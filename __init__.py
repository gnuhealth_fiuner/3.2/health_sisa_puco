# -*- coding: utf-8 -*-
#This file is part health_sisa_puco module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from trytond.pool import Pool
from .party import *
from .wizard import *


def register():
    Pool.register(
        Party,
        PartyIdentifier,
        PucoDataStart,
        PatientPucoDataStart,
        module='health_sisa_puco', type_='model')
    Pool.register(
        PucoData,
        PatientPucoData,
        module='health_sisa_puco', type_='wizard')
