# -*- coding: utf-8 -*-
#This file is part health_sisa_puco module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys

from trytond.model import ModelView
from trytond.pyson import Eval, Not, Bool
from trytond.pool import PoolMeta


__all__ = ['Party', 'PartyIdentifier']


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        cls._buttons.update({
            'get_puco_data': {
                'invisible': Not(Bool(Eval('is_person'))),
                },
        })

    @classmethod
    def check_xml_record(cls, records, values):
        return True

    @classmethod
    @ModelView.button_action('health_sisa_puco.wizard_puco_data')
    def get_puco_data(cls, parties):
        pass

    @classmethod
    def create(cls, vlist):
        # avoid loading CUIT if 'party_ar' module is installed
        if 'trytond.modules.party_ar' in sys.modules:
            for values in vlist:
                if values.get('is_insurance_company') is True and \
                        not values.get('iva_condition'):
                    values['iva_condition'] = 'no_alcanzado'
        return super(Party, cls).create(vlist)


class PartyIdentifier(metaclass=PoolMeta):
    __name__ = 'party.identifier'

    @classmethod
    def get_types(cls):
        types = super(PartyIdentifier, cls).get_types()
        types.append(('ar_rnos', 'RNOS'))
        return types
